using AddDummyFile;
using NUnit.Framework;
using System.IO;

namespace Tests
{
    public class Tests
    {
        public string path = @"C:\Disk_D\Artelogic";
        [SetUp]
        public void Setup()
        {
            FileSettings fileSettings = new FileSettings("test.txt");
            fileSettings.writeFile(path);
        }

        [Test]
        public void isCreateFile()
        {

            string[] directories = Directory.GetDirectories(path);
            foreach (var directory in directories)
            {
                Assert.IsTrue(File.Exists(directory + "\\test.txt"));
            }
        }

        [Test]
        public void isFilesAreSame()
        {
            string file1 = readFile(path + "\\Task1_Kovtunov\\test.txt");
            string file2 = readFile(path + "\\TestFolder\\test.txt");
            Assert.AreEqual(file1, file2);
        }

        [Test]
        public void isFileNotEmpty()
        {
            string file1 = readFile(path + "\\Task1_Kovtunov\\test.txt");
            Assert.IsTrue(file1.Length != 0);
        }

        private string readFile(string path)
        {
            string line;
            using (StreamReader sr = new StreamReader(path))
            {
                line = sr.ReadToEnd();
            }
            return line;
        }
    }
}