﻿using System;
using System.IO;

namespace AddDummyFile
{
    public class FileSettings
    {
        private readonly string fileName;
        public FileSettings( string fileName)
        {
            this.fileName = fileName;
        }

        public void writeFile(string path)
        {
            try
            {
                foreach (var directory in Directory.GetDirectories(path))
                {
                    string filePath = directory + @"\" + fileName;
                    using (StreamWriter sw = (File.Exists(filePath)) ? File.AppendText(filePath) : File.CreateText(filePath))
                    {
                        sw.WriteLine(DateTime.Now);
                    }
                    writeFile(directory);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Disk_D\Artelogic\TestFolder";
            string fileName = "dummy.txt";

            FileSettings directorySettings = new FileSettings(fileName);
            directorySettings.writeFile(path);

            Console.ReadLine();
        }
    }
}
